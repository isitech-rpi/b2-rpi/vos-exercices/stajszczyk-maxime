#pragma once

#include <string>
#include <iostream>
using namespace std;

class Cours {
private:
    string matiere;
    string promo;

public:
    Cours(void);

    Cours(string matiere, string promo);

    ~Cours(void);

    std::string getMatiere() { return this->matiere; }
    std::string getPromo() { return this->promo; }
    std::string getMatiere() const { return this->matiere; }
    std::string getPromo() const { return this->promo; }

    void setPromo(std::string promo);

    void Enseigner() const;
};

