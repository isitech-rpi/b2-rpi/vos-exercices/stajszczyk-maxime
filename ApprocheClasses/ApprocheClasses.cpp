#include <iostream>
#include <Windows.h>
#include "Cours.h"
using namespace std;

int main()
{
    SetConsoleOutputCP(CP_UTF8);
    /*   Cours cours1;

       Cours cours2("React Native", "B2 DEV");

       cout << cours2.getMatiere() << endl << cours2.getPromo();
       cours2.Enseigner();

       cours2.setPromo("B3 DEV RPI");

       cout << "Matière du cours2 : " << cours2.getMatiere() << endl;
       cout << "Promotion du cours2 : " << cours2.getPromo() << endl;*/

       // Manière statique
    Cours matiere_1("C++", "B2-RPI");
    matiere_1.Enseigner();

    //manière dynamique
    Cours* matiere_2 = new Cours("PHP", "3olen");
    matiere_2->Enseigner();

    //Instancie un tableau statique
    Cours lesCours_1[2] = { Cours("UWP", "B3-WTech"), Cours("UML", "B2-devWeb") };
    for (int i = 0; i < 2; ++i) {
        lesCours_1[i].Enseigner();
    }

    //Instancie un tableau dynamique
    Cours* lesCours_2 = new Cours[2]{ Cours("Git", "3olen"), Cours("PHP", "B2-WTech") };
    for (int i = 0; i < 2; ++i) {
        lesCours_2[i].Enseigner();
    }

    //Modifie les promo
    lesCours_1[0].setPromo("B2-DEV-RPI");
    lesCours_2[0].setPromo("B3-DEV-RPI");

    //Affiche les modif des promo
    cout << "lesCours_1 :" << endl;
    for (int i = 0; i < 2; ++i) {
        cout << "Cours " << i + 1 << " : " << lesCours_1[i].getMatiere() << "    " << lesCours_1[i].getPromo() << endl;
    }

    cout << "lesCours_2 : " << endl;
    for (int i = 0; i < 2; ++i) {
        cout << "Cours " << i + 1 << " : " << lesCours_2[i].getMatiere() << "    " << lesCours_2[i].getPromo() << endl;
    }

    //Destruction du constructeur avec pointeur
    delete matiere_2;
    delete[] lesCours_2;
}