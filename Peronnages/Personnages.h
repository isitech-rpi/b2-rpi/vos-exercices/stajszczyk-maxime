#pragma once
#include <string>
#include <iostream>
using namespace std;

class Personnage {
private:
    string nom;

protected:
    double vie;
    int niveau;

    bool Defendre(Personnage* ennemi) {
        if (vie > 0) {
            vie -= (ennemi->vie * ennemi->niveau) / 20.0;
            if (vie < 0) vie = 0;
            return true;
        }
        return false;
    }

public:
    Personnage(const string& nom = "", double vie = 10.0, int niveau = 1)
        : nom(nom), vie(vie), niveau(niveau) {}

    ~Personnage() {}

    void afficherPerso() const {
        cout << "Nom: " << nom << ", Vie: " << vie << ", Niveau: " << niveau << endl;
    }

    bool Attaquer(Personnage* ennemi) {
        if (ennemi->vie > 0) {
            ennemi->vie -= (vie * niveau) / 20.0;
            if (ennemi->vie < 0) ennemi->vie = 0;
            ennemi->Defendre(this);
            return true;
        }
        return false;
    }

    string getNom() const { return nom; }
};

class Heros : public Personnage {
public:
    Heros(const string& nom, int niveau = 2)
        : Personnage(nom, 10.0, niveau) {}

    void afficherPerso() const {
        cout << "Heros - ";
        Personnage::afficherPerso();
    }
};


