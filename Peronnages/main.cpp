#include "Personnages.h"
#include <memory> 

int main() {
    
    Personnage guethenoc("Guethenoc");
    guethenoc.afficherPerso();

    unique_ptr<Personnage> balcmeg = make_unique<Personnage>("Balcmeg");
    balcmeg->afficherPerso();

    Heros gollum("Gollum");
    gollum.afficherPerso();

    unique_ptr<Heros> frodon = make_unique<Heros>("Frodon", 10);
    frodon->afficherPerso();

    cout << "\nArthur attaque Gollum:" << endl;
    guethenoc.Attaquer(&gollum);
    guethenoc.afficherPerso();
    gollum.afficherPerso();

    cout << "\nFrodon attaque Balcmeg:" << endl;
    frodon->Attaquer(balcmeg.get());
    frodon->afficherPerso();
    balcmeg->afficherPerso();

    return 0;
}