// HelloWorld.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

//#include <iostream>
//using namespace std;

//int main()
//{   
//    auto ouiTest = "Oui Test !";
//    cout << "Hello World!\n";
//}

#include <iostream>
#include <iomanip>
#undef max
using namespace std;

//1.1

int a = 1;
int b = 5;
double f = 3.14;
char c('C');
char d;

//1.2

int i1 = 1;
int i2 = 3;
int r1;
double d1 = 1;
double d2 = 3;
double r2;

//1.3

int a2 = 3;
int b2 = 10;
int* p;


void exo1() {
    // 1.1 Déclaration, affectation, affichage

    cout << "Exercice 1.1" << endl;

    cout << "a = " << a << "\nAdress = " << hex << &a << endl;
    cout << "b = " << b << "\nAdress = " << hex << &b << endl;
    cout << "f = " << f << "\nAdress = " << hex << &f << endl;
    cout << "c = " << c << "\nAdress = " << hex << &c << endl;
    cout << "d = " << d << "\nAdress = " << hex << &d << endl;

    d = 'D';
    cout << "d = " << d << "\nAdress = " << hex << &d << endl;

    decltype(a) temp = a;
    a = b;
    b = temp;
    cout << "a = " << a;
    cout << "b = " << b;

    temp = a;
    a = f;
    f = temp;
    cout << "a = " << a;
    cout << "f = " << f;

    a = f;
    cout << "a = " << a;
    cout << "f = " << f;

    d = 90;
    cout << "d = " << d << "\nnumeric value = " << static_cast<int>(d) << endl;

    d += 255;
    cout << "d = " << d << "\nnumeric value = " << static_cast<int>(d) << endl;

    // 1.2 Operation sur les nombres

    cout << "Exercice 1.2" << endl;

    r1 = (i1 / i2);
    cout << "r1 = " << r1 << endl;

    r2 = (i1 / i2);
    cout << "r2 = (i1 / i2)\n" << "r2 = " << r2 << endl;

    r2 = (d1 / i2);
    cout << "r2 = (d1 / i2)\n" << "r2 = " << r2 << endl;

    r2 = (i1 / d2);
    cout << "r2 = (i1 / d2)\n" << "r2 = " << r2 << endl;

    r2 = (d1 / d2);
    cout << "r2 = (d1 / d2)\n" << "r2 = " << r2 << endl;

    // 1.3 Pointeur

    cout << "Exercice 1.3" << endl;

    cout << "p = " << dec << p << "\nAdress : " << hex << &p << endl;
    p = &b2;
    cout << "p = " << dec << p << "\nValeur : " << *p << "\nAdress : " << hex << &p << endl;
    p = &a2;
    cout << "p = " << dec << p << "\nValeur : " << *p << "\nAdress : " << hex << &p << endl;
    *p *= 2;
    cout << "p = " << dec << p << "\nValeur : " << *p << "\nAdress : " << hex << &p << endl;
    p += 1;
    cout << "p = " << dec << p << "\nValeur : " << *p << "\nAdress : " << hex << &p << endl;
};

void exo2_1() {

    // 2.1 Conditions simples

    cout << "Exercice 2.1" << endl;

    int heure;
    int minute;

    cout << " Veuillez renseigner une heure (entre 0 et 23) : ";
    cin >> heure;
    while (std::cin.fail() || heure < 0 || heure > 23) {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Veuillez rentrer un NOMBRE CORRECT ! (entre 0 et 23) \n";
        cin >> heure;
    }
    cout << endl;
    cout << " Veuillez renseigner les minutes (entre 0 et 59) : ";
    cin >> minute;
    while (std::cin.fail() || minute < 0 || minute > 59) {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Veuillez rentrer un NOMBRE CORRECT ! (entre 0 et 59) \n";
        cin >> minute;
    }
    cout << endl;
    cout << std::setfill('0') << std::setw(2) << heure << ":" << std::setfill('0') << std::setw(2) << minute << endl;
    minute++;
    cout << "Voici l'heure predite : " << std::setfill('0') << std::setw(2) << heure << ":" << std::setfill('0') << std::setw(2) << minute << endl;
}

void exo2_2_1() {

    // 2.2 Conditions multiples

    cout << "Exercice 2.2 (If version)" << endl;

    int age;

    cout << "Veuillez renseigner votre age : \n";
    cin >> age;

    if (6 <= age && age <= 9) {
        cout << "Vous etes un pre-poussin. \n";
    }
    if (10 <= age && age <= 11) {
        cout << "Vous etes un poussin. \n";
    }
    if (12 <= age && age <= 13) {
        cout << "Vous etes un benjamin. \n";
    }
    if (14 <= age && age <= 15) {
        cout << "Vous etes un minime. \n";
    }
    if (16 <= age && age <= 17) {
        cout << "Vous etes un cadet. \n";
    }
    if (18 <= age) {
        cout << "Vous etes un senior. \n";
    }
    
}

void exo2_2_2() {
    
    // 2.2 Conditions multiples

    cout << "Exercice 2.2 (Switch version)" << endl;

    int age;

    cout << "Veuillez renseigner votre age : \n";
    cin >> age;
    while (std::cin.fail() || age < 6 || age > 100) {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Veuillez rentrer un AGE REEL pour faire du sport (Nondeudwew) ! (entre 6 et 100) \n";
        cin >> age;
    }

    switch (age) {
    case 6: case 7: case 8: case 9:
        cout << "Vous etes un pre-poussin. \n";
        break;
    case 10: case 11:
        cout << "Vous etes un poussin. \n";
        break;
    case 12: case 13:
        cout << "Vous etes un benjamin. \n";
        break;
    case 14: case 15:
        cout << "Vous etes un minime. \n";
        break;
    case 16: case 17:
        cout << "Vous etes un cadet. \n";
        break;
    default:
        cout << "Vous etes un senior. \n";
        break;
    }
}

void exo2_3() {

    // 2.3 Itérations

    cout << "Exercice 2.3" << endl;

    double note, somme = 0;
    int compteur = 0;

    cout << "Veuillez renseigner les notes (tapez -1 pour terminer) : " << endl;
    while (true) {
        cout << "Note " << compteur + 1 << " : ";
        cin >> note;

        if (note == -1) {
            break;
        }

        if (note >= 0 && note <= 20) {
            somme += note;
            compteur++;
        }
        else {
            cout << "Note invalide, veuillez entrer une note entre 0 et 20." << endl;
        }
    }

    if (compteur > 0) {
        double moyenne = somme / compteur;
        cout << "La moyenne des notes est : " << moyenne << endl;
    }
    else {
        cout << "Aucune note valide n'a été saisie." << endl;
    }

}

int saisirEntierEntre(int minimum, int maximum) {
    int nombre;
    char message;
    cout << "Veuillez renseigner un chiffre (entre " << minimum << " et " << maximum << ") \n" << endl;
    cin >> nombre;
    while (std::cin.fail() || maximum < nombre || minimum > nombre) {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Veuillez rentrer un NOMBRE CORRECT ! (entre " << minimum << " et " << maximum << ") \n" << endl;
        cin >> nombre;
    }
    return nombre;
}



void exo3_1() {

    // 3.1 Saisie d'un nombre

    cout << "Exercice 3.1" << endl;
    
    saisirEntierEntre(5, 100);

}

int permute(int first, int second ) {
    int temp;

    
    temp = first;
    first = second;
    second = temp;
}

void exo3_3() {

    // 3.3 Permute

    cout << "Exercice 3.3" << endl;

//   permute();

}

void exo4()
{
    // 4 Tableau

    cout << "Exercice 4" << endl;

    system("cls");	//  Efface l'écran

    cout << endl;
    cout << "------------" << endl;
    cout << "|   MENU   |" << endl;
    cout << "------------" << endl << endl;
    cout << "1) Remplir" << endl;
    cout << "2) Copier" << endl;
    cout << "3) Trier" << endl;
    cout << "4) Quitter" << endl << endl << endl;
}

void pause() {
    system("pause");
}

int choixPartie(int partie) {
    cin >> partie;
    while (std::cin.fail() || partie < 1 || partie > 3)
    {
        cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << endl << " Saisie incorrect, veuillez saisir un nombre correct (1, 2 ou 3): ";
        cin >> partie;
    }
    system("cls");
    return partie;
}

void remplirTab(int* tab, int lngTab) {
    for (int i = 0; i < lngTab; i++) {
        cout << "Element " << i + 1 << " du tableau : ";
        cin >> tab[i];

        while (std::cin.fail()) {
            cout << "Saisie incorrect, veuillez saisir un nombre correct : " << endl;
            cin >> tab[i];
        }
    }

}

int* copie(const int* src, int lngTab) {
    int* newTab = new int[lngTab];
    for (int i = 0; i < lngTab; i++) {
        newTab[i] = src[i];
    }
    return newTab;
}

void sortTab(int* tab1, int lngTab, bool ordreCroissant = true) {
    for (int i = 0; i < lngTab - 1; i++) {
        int indExt = i;
        for (int j = i + 1; j < lngTab; j++) {
            if (ordreCroissant ? tab1[j] < tab1[indExt] : tab1[j] > tab1[indExt]) {
                indExt = j;
            }
        }
        if (indExt != i) {
            int temp = tab1[i];
            tab1[i] = tab1[indExt];
            tab1[indExt] = temp;
        }
    }
}

int main()
{
    std::cout << "1. Tableau\n" << endl;
    bool quit = false;
    int choix = 0;
    const int lngTab = 5;
    int tab1[lngTab];
    int* tabCopie;
    bool sorting = false;
    SetConsoleOutputCP(CP_UTF8);

    //  Initialisation pour le générateur de nombres aléatoires
    srand((unsigned int)std::time(0));

    do
    {
        //  Affiche le menu et lit le choix de l'utilisateur
        afficheMenu();
        choix = choixPartie(0);

        switch (choix)	//  Réalise l'action choisie par l'utilisateur
        {
        case 1:

            cout << "\nOn remplie le tableau d'entier de taille " << lngTab << endl;
            remplirTab(tab1, lngTab);

            cout << endl << endl << "Votre tableau : ";
            for (int i = 0; i < lngTab; i++) {
                std::cout << tab1[i] << ", ";
            }
            cout << endl;
            pause();
            break;
        case 2:
            tabCopie = copie(tab1, lngTab);
            cout << endl << endl << "Tableau copié : ";
            for (int i = 0; i < lngTab; i++) {
                std::cout << tabCopie[i] << ", ";
            }
            cout << endl;

            pause();
            break;
        case 3:

            cout << "Voulez vous triez votre tableau par order croissant (1), ou décroissant (2)" << endl;
            cin >> choix;
            if (choix == 1) {
                sorting = true;
            }
            else { sorting = false; }
            sortTab(tab1, lngTab, sorting);
            pause();
            break;
        case 4:
            quit = true;
            break;
        }
    } while (!quit);

}


int main()
{
    while (true) {
        int exo;

        cout << "Choisir l'exo \n - 1 pour l'exercice 1 entier \n - 21 pour l'exercice 2.1 \n - 221 pour l'exercice 2.2 version If \n - 222 pour l'exercice 2.2 version Switch \n - 23 pour l'exercice 2.3 \n - 31 pour l'exercice 3.1 \n Faites votre choix: ";
        cin >> exo;
        switch (exo) {
        case 1:
            exo1();
            break;
        case 21:
            exo2_1();
            break;
        case 221:
            exo2_2_1();
            break;
        case 222:
            exo2_2_2();
            break;
        case 23:
            exo2_3();
            break;
        case 31:
            exo3_1();
            break;
        case 4:
            exo4();
            break;
        }
    }
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
